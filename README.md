## Ldap Query

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 7.1+
* [Composer](https://getcomposer.org/)

### Pieces
* `LdapQuery\Contracts\Backend` - handles setting up defaults for the connection and returning
 query results, uses `LdapQuery\Contracts\Client` and `LdapQuery\Contracts\Result`
* `LdapQuery\Contracts\Builder` - query builder, uses `LdapQuery\Contracts\Filter` to create backend specific
query/filter
* `LdapQuery\Contracts\Client` - proxy to actual ldap_ functions (or whatever the lower level backend is)
* `LdapQuery\Contracts\Model` - simple object to represent ldap entry, returned from `LdapQuery\Contracts\Result`
* `LdapQuery\Contracts\Result` - uses `LdapQuery\Contracts\Builder` to create a form that is useful to
`LdapQuery\Contracts\Client` and returns the result from the client in `LdapQuery\Contracts\Model` form
* `LdapQuery\Contracts\Filter` - used by `LdapQuery\Contracts\Builder` to set up the possible filters

`LdapQuery\Opinionated\ActiveDirectory` is an example of how the LdapQuery can be wired for AD.  It can all be 
done manually or through DI as well.

### Examples

```
$o = new ActiveDirectory([
  'host' => 'foo.bar.com',
]);
$lq = $o->getLdapQuery();
if ($lq->authenticate('user', 'password')) {
  //do something when the user is successfully authenticated
}
$user = $lq->findUser('user');
$another = $lq->one('uid', 'some_user');
$many = $lq->many('l', 'some_location');
```
