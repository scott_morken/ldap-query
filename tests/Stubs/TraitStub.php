<?php
namespace Tests\LdapQuery\Stubs;

class TraitStub
{

    use \LdapQuery\Traits\SetOptionsViaDefaults;

    public static function getDefaults(): array
    {
        return [
            'foo' => 'bar',
            'fizz' => 'bazz',
        ];
    }
}
