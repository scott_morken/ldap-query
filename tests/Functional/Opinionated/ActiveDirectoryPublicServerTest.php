<?php

namespace Tests\LdapQuery\Functional\Opinionated;

use LdapQuery\Clients\ClientException;
use LdapQuery\Opinionated\ActiveDirectory;
use Tests\LdapQuery\Functional\BaseTestCase;

class ActiveDirectoryPublicServerTest extends BaseTestCase
{

    public function testBindAsUser(): void
    {
        $this->checkRun();
        $sut = new ActiveDirectory($this->getTestOptions());
        $q = $sut->getLdapQuery();
        $this->assertTrue($q->authenticate('gauss', 'password'));
    }

    public function testBindAsUserFalse(): void
    {
        $this->checkRun();
        $sut = new ActiveDirectory($this->getTestOptions());
        $q = $sut->getLdapQuery();
        $this->assertFalse($q->authenticate('gauss', 'nope'));
    }

    public function testBindAsUserOtherErrorThrowsException(): void
    {
        $this->checkRun();
        $options = $this->getTestOptions();
        $options['host'] = 'nohost.local';
        $sut = new ActiveDirectory($options);
        $q = $sut->getLdapQuery();
        $this->expectException(ClientException::class);
        $this->expectExceptionMessage("Can't contact LDAP server");
        $q->authenticate('gauss', 'password');
    }

    public function testFindUserAfterBind(): void
    {
        $this->checkRun();
        $sut = new ActiveDirectory($this->getTestOptions());
        $q = $sut->getLdapQuery();
        $q->authenticate('gauss', 'password');
        $result = $q->findUser('gauss');
        $this->assertEquals('Carl Friedrich Gauss', $result->cn);
    }
}
