<?php

namespace Tests\LdapQuery\Functional\Opinionated;

use LdapQuery\Contracts\Model;
use LdapQuery\Opinionated\ActiveDirectory;
use Tests\LdapQuery\Functional\BaseTestCase;

class ActiveDirectoryPrivateServerTest extends BaseTestCase
{

    public function testWithSsl(): void
    {
        $this->checkRun();
        $options = $this->getPrivateOptions();
        $options['port'] = 636;
        $options['client_options']['ssl'] = true;
        $options['client_options']['post_connect_options']['LDAP_OPT_X_TLS_REQUIRE_CERT'] = LDAP_OPT_X_TLS_NEVER;
        $o = new ActiveDirectory($options);
        $sut = $o->getLdapQuery();
        $result = $sut->findUser($options['search_user']);
        $this->assertInstanceOf(Model::class, $result);
    }

    public function testWithTls(): void
    {
        $this->checkRun();
        $options = $this->getPrivateOptions();
        $options['client_options']['start_tls'] = true;
        $options['client_options']['post_connect_options']['LDAP_OPT_X_TLS_REQUIRE_CERT'] = LDAP_OPT_X_TLS_NEVER;
        $o = new ActiveDirectory($options);
        $sut = $o->getLdapQuery();
        $result = $sut->findUser($options['search_user']);
        $this->assertInstanceOf(Model::class, $result);
    }

    protected function getPrivateOptions(): array
    {
        return require __DIR__.'/local.php';
    }
}
