<?php

namespace Tests\LdapQuery\Functional\Clients;

use LdapQuery\Clients\ClientException;
use LdapQuery\Clients\Ldap;
use Tests\LdapQuery\Functional\BaseTestCase;

class LdapTest extends BaseTestCase
{

    protected string $connectUrl = 'ldap.forumsys.com';

    public function testBind(): void
    {
        $this->checkRun();
        $sut = new Ldap();
        $sut->connect($this->connectUrl);
        $sut->setOption(LDAP_OPT_PROTOCOL_VERSION, 3);
        $sut->bind();
        $this->assertTrue($sut->isBound());
        $sut->close();
    }

    public function testConnect(): void
    {
        $this->checkRun();
        $sut = new Ldap();
        $this->assertTrue($sut->connect($this->connectUrl));
        $this->assertTrue($sut->isConnected());
        $sut->close();
    }

    public function testConnectFailureThrowsException(): void
    {
        $this->checkRun();
        $sut = new Ldap();
        $sut->connect('foo.bar.local');
        $this->expectException(ClientException::class);
        $this->expectExceptionMessage("Can't contact LDAP server");
        $sut->bind();
    }

    public function testSearch(): void
    {
        $this->checkRun();
        $sut = new Ldap();
        $sut->connect($this->connectUrl);
        $sut->setOption(LDAP_OPT_PROTOCOL_VERSION, 3);
        $sut->bind('cn=read-only-admin,dc=example,dc=com', 'password');
        $searchResults = $sut->search('dc=example,dc=com', '(sn=Eu*)');
        $results = $sut->getEntries($searchResults);
        $this->assertEquals(2, $results['count']);
        $sut->close();
    }
}
