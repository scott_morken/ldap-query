<?php

namespace Tests\LdapQuery\Functional;

use PHPUnit\Framework\TestCase;
use Tests\LdapQuery\Traits\ShouldRun;

abstract class BaseTestCase extends TestCase
{

    use ShouldRun;

    protected bool $run = true;
}
