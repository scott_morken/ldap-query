<?php

namespace Tests\LdapQuery\Traits;

trait ShouldRun
{

    protected function checkRun(): void
    {
        if (!$this->shouldRun()) {
            $this->markTestSkipped('Requires live connection.');
        }
    }

    protected function getTestOptions(): array
    {
        return [
            'host' => 'ldap.forumsys.com',
            'port' => 389,
            'base_dn' => 'dc=example,dc=com',
            'bind_user' => 'cn=read-only-admin,dc=example,dc=com',
            'bind_password' => 'password',
            'bind_filter' => 'uid=%s,dc=example,dc=com',
            'rebind' => true,
            'follow_referrals' => 1,
            'filters' => [
                'person' => [
                    'category' => null,
                    'samaccounttype' => null,
                    'uid_attribute' => 'uid',
                ],
            ],
        ];
    }

    protected function shouldRun(): bool
    {
        return property_exists($this, 'run') && $this->run === true;
    }
}
