<?php

namespace Tests\LdapQuery\Unit\Backends;

use LdapQuery\Backends\ActiveDirectory;
use LdapQuery\Contracts\Backend;
use LdapQuery\Contracts\Client;
use LdapQuery\LdapQueryException;
use LdapQuery\Models\VO\Model;
use LdapQuery\Results\Ldap;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class ActiveDirectoryTest extends TestCase
{

    protected Client|m\Mock|null $client = null;

    public function testBindDefaults(): void
    {
        $sut = $this->getSut();
        $this->getClient()->shouldReceive('isConnected')
             ->times(3)
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->times(2)
             ->with(null, null)
             ->andReturn(true);
        $this->getClient()->shouldReceive('isBound')
             ->times(1)
             ->andReturn(true);
        $this->assertTrue($sut->bind());
    }

    public function testBindWithRebindThrowsException(): void
    {
        $sut = $this->getSut();
        $this->getClient()->shouldReceive('isConnected')
             ->times(3)
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->times(2)
             ->with(null, null)
             ->andReturnValues([true, false]);
        $this->getClient()->shouldReceive('isBound')
             ->times(1)
             ->andReturn(true);
        $this->getClient()->shouldReceive('error')
             ->once()
             ->andReturn('Rebind error');
        $this->expectException(LdapQueryException::class);
        $sut->bind();
    }

    public function testBindWithUserAndPassword(): void
    {
        $sut = $this->getSut(['rebind' => false]);
        $this->getClient()->shouldReceive('isConnected')
             ->times(2)
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->once()
             ->with('foo@domain.org', 'bar')
             ->andReturn(true);
        $this->assertTrue($sut->bind('foo', 'bar'));
    }

    public function testBindWithUserAndPasswordAndRebind(): void
    {
        $sut = $this->getSut();
        $this->getClient()->shouldReceive('isConnected')
             ->times(3)
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->once()
             ->with('foo@domain.org', 'bar')
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->once()
             ->with(null, null)
             ->andReturn(true);
        $this->getClient()->shouldReceive('isBound')
             ->times(1)
             ->andReturn(true);
        $this->assertTrue($sut->bind('foo', 'bar'));
    }

    public function testBindWithUserAndPasswordAndRebindWithBindUser(): void
    {
        $sut = $this->getSut(['bind_user' => 'biz', 'bind_password' => 'buzz']);
        $this->getClient()->shouldReceive('isConnected')
             ->times(3)
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->once()
             ->with('foo@domain.org', 'bar')
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->once()
             ->with('biz@domain.org', 'buzz')
             ->andReturn(true);
        $this->getClient()->shouldReceive('isBound')
             ->times(1)
             ->andReturn(true);
        $this->assertTrue($sut->bind('foo', 'bar'));
    }

    public function testBindWithoutRebind(): void
    {
        $sut = $this->getSut(['rebind' => false]);
        $this->getClient()->shouldReceive('isConnected')
             ->times(2)
             ->andReturn(true);
        $this->getClient()->shouldReceive('bind')
             ->once()
             ->with(null, null)
             ->andReturn(true);
        $this->assertTrue($sut->bind());
    }

    protected function getClient(): Client
    {
        if (!$this->client) {
            $this->client = m::mock(Client::class);
        }
        return $this->client;
    }

    protected function getSut(array $options = []): Backend
    {
        return new ActiveDirectory($this->getClient(), new Ldap(new Model()), $options);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
