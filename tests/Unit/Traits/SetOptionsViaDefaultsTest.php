<?php
namespace Tests\LdapQuery\Unit\Traits;

use PHPUnit\Framework\TestCase;
use Tests\LdapQuery\Stubs\TraitStub;

class SetOptionsViaDefaultsTest extends TestCase
{

    public function testGetMissingValueWithDefault(): void
    {
        $sut = new TraitStub();
        $sut->setOptionsViaDefaults();
        $this->assertEquals('bizz', $sut->getOptionByKey('nope', 'bizz'));
    }

    public function testGetModifiedValueFromConfig(): void
    {
        $sut = new TraitStub();
        $sut->setOptionsViaDefaults(['foo' => 'far']);
        $this->assertEquals('far', $sut->getOptionByKey('foo'));
    }

    public function testSetFromDefaultsIsDefaults(): void
    {
        $sut = new TraitStub();
        $sut->setOptionsViaDefaults();
        $this->assertEquals(TraitStub::getDefaults(), $sut->getOptions());
    }

    public function testSetFromDefaultsWithValueNotEqual(): void
    {
        $sut = new TraitStub();
        $sut->setOptionsViaDefaults(['foo' => 'far']);
        $this->assertNotEquals(TraitStub::getDefaults(), $sut->getOptions());
    }
}
