<?php

namespace Tests\LdapQuery\Unit\Models\VO;

use LdapQuery\Models\VO\Model;
use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase
{

    public function testMagicGet(): void
    {
        $sut = new Model(['foo' => 'bar', 'fizz' => 'baz']);
        $this->assertEquals('bar', $sut->foo);
    }

    public function testMagicGetIsNullWhenNotExists(): void
    {
        $sut = new Model(['foo' => 'bar', 'fizz' => 'baz']);
        $this->assertNull($sut->buz);
    }

    public function testMagicSet(): void
    {
        $sut = new Model();
        $sut->foo = 'bar';
        $this->assertEquals(['foo' => 'bar'], $sut->toArray());
    }

    public function testNewModelIsNewModel(): void
    {
        $sut = new Model();
        $this->assertNotSame($sut, $sut->newModel([]));
    }

    public function testNewModelReturnsModel(): void
    {
        $sut = new Model();
        $this->assertInstanceOf(\LdapQuery\Contracts\Model::class, $sut->newModel([]));
    }

    public function testToArrayReturnsAttributes(): void
    {
        $sut = new Model(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $sut->toArray());
    }

    public function testToArrayWithAttributesReturnsThoseAttributes(): void
    {
        $sut = new Model(['foo' => 'bar', 'fizz' => 'baz']);
        $this->assertEquals(['fizz' => 'baz'], $sut->toArray(['fizz']));
    }
}
