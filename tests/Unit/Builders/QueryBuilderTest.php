<?php

namespace Tests\LdapQuery\Unit\Builders;

use LdapQuery\Contracts\Builder;
use LdapQuery\Contracts\Filter;
use LdapQuery\Query\Builders\QueryBuilder;
use LdapQuery\Query\Filters\InvalidFilterException;
use PHPUnit\Framework\TestCase;

class QueryBuilderTest extends TestCase
{

    public function testChainingWheresAddsToWheres(): void
    {
        $sut = $this->getSut();
        $r = $sut->where('foo')->where('bar')->orWhere('baz')->where('bat');
        $this->assertCount(4, $sut->getWheres());
    }

    public function testGetAndWheresOnlyReturnsAnds(): void
    {
        $sut = $this->getSut();
        $r = $sut->where('foo')->where('bar')->orWhere('baz');
        $this->assertCount(2, $sut->getWheres($sut::WHERE_AND));
    }

    public function testGetFilterIsFilter(): void
    {
        $sut = $this->getSut();
        $sym = $sut->getFilter();
        $this->assertInstanceOf(Filter::class, $sym);
    }

    public function testGetOrWheresOnlyReturnsOrs(): void
    {
        $sut = $this->getSut();
        $r = $sut->where('foo')->where('bar')->orWhere('baz');
        $this->assertCount(1, $sut->getWheres($sut::WHERE_OR));
    }

    public function testGetQueryAndOr(): void
    {
        $sut = $this->getSut();
        $sut->where('foo', 'bar')->orWhere('biz', 'bat');
        $this->assertEquals('(&(foo=bar)(|(biz=bat)))', $sut->getQuery());
    }

    public function testGetQueryOneWhere(): void
    {
        $sut = $this->getSut();
        $sut->where('foo', 'bar');
        $this->assertEquals('(foo=bar)', $sut->getQuery());
    }

    public function testGetQueryOrWhere(): void
    {
        $sut = $this->getSut();
        $sut->orWhere('foo', 'bar');
        $this->assertEquals('(|(foo=bar))', $sut->getQuery());
    }

    public function testGetQueryTwoAndTwoOr(): void
    {
        $sut = $this->getSut();
        $sut->where('foo', 'bar')->where('buzz', 'bizz')->orWhere('biz', 'bat')->orWhere('bazz', 'bozz');
        $this->assertEquals('(&(foo=bar)(buzz=bizz)(|(biz=bat)(bazz=bozz)))', $sut->getQuery());
    }

    public function testGetQueryTwoAndWheres(): void
    {
        $sut = $this->getSut();
        $sut->where('foo', 'bar')->where('biz', 'bat');
        $this->assertEquals('(&(foo=bar)(biz=bat))', $sut->getQuery());
    }

    public function testGetQueryTwoOrWheres(): void
    {
        $sut = $this->getSut();
        $sut->orWhere('foo', 'bar')->orWhere('biz', 'bat');
        $this->assertEquals('(&(|(foo=bar)(biz=bat)))', $sut->getQuery());
    }

    public function testGetQueryWithNotEquals(): void
    {
        $sut = $this->getSut();
        $sut->where('foo', 'bar', Filter::NOT_EQUALS_FILTER);
        $this->assertEquals('(!(foo=bar))', $sut->getQuery());
    }

    public function testGetQueryWithWildcard(): void
    {
        $sut = $this->getSut();
        $sut->where('foo', null, Filter::WILDCARD_FILTER);
        $this->assertEquals('(foo=*)', $sut->getQuery());
    }

    public function testOrWhereAddsToWheres(): void
    {
        $sut = $this->getSut();
        $r = $sut->orWhere('foo');
        $this->assertCount(1, $sut->getWheres());
    }

    public function testOrWhereInvalidOperatorThrowsException(): void
    {
        $sut = $this->getSut();
        $this->expectException(InvalidFilterException::class);
        $r = $sut->where('foo', 'bar', 999);
    }

    public function testOrWhereReturnsThis(): void
    {
        $sut = $this->getSut();
        $r = $sut->orWhere('foo');
        $this->assertEquals($r, $sut);
    }

    public function testSelectAddsToSelects(): void
    {
        $selects = ['foo', 'bar', 'biz'];
        $sut = $this->getSut();
        $sut->select($selects);
        $this->assertCount(3, $sut->getSelects());
    }

    public function testSelectReturnsThis(): void
    {
        $sut = $this->getSut();
        $r = $sut->select([]);
        $this->assertEquals($r, $sut);
    }

    public function testStartsWithFilter(): void
    {
        $sut = $this->getSut();
        $r = $sut->where('foo', 'bar', Filter::STARTS_WITH_FILTER);
        $this->assertEquals('(foo=bar*)', $r->getQuery());
    }

    public function testWhereAddsToWheres(): void
    {
        $sut = $this->getSut();
        $r = $sut->where('foo');
        $this->assertCount(1, $sut->getWheres());
    }

    public function testWhereInvalidOperatorThrowsException(): void
    {
        $sut = $this->getSut();
        $this->expectException(InvalidFilterException::class);
        $r = $sut->where('foo', 'bar', 999);
    }

    public function testWhereReturnsThis(): void
    {
        $sut = $this->getSut();
        $r = $sut->where('foo');
        $this->assertEquals($r, $sut);
    }

    protected function getSut(): Builder
    {
        return new QueryBuilder(new \LdapQuery\Query\Filters\ActiveDirectory());
    }
}
