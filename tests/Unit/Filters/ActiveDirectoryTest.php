<?php

namespace Tests\LdapQuery\Unit\Filters;

use LdapQuery\Contracts\Filter;
use LdapQuery\Query\Filters\ActiveDirectory;
use LdapQuery\Query\Filters\InvalidFilterException;
use PHPUnit\Framework\TestCase;

class ActiveDirectoryTest extends TestCase
{

    public function testFilterIsAllowedIsTrue(): void
    {
        $sut = new ActiveDirectory();
        $this->assertTrue($sut->isAllowedFilter(Filter::EQUALS_FILTER));
    }

    public function testFilterNotAllowedThrowsException(): void
    {
        $sut = new ActiveDirectory();
        $this->expectException(InvalidFilterException::class);
        $sut->isAllowedFilter(Filter::AND_FILTER);
    }

    public function testGetFilter()
    {
        $sut = new ActiveDirectory();
        $this->assertEquals('(&%s)', $sut->getFilter(Filter::AND_FILTER));
    }

    public function testGetFilterNotExistsThrowsException(): void
    {
        $sut = new ActiveDirectory();
        $this->expectException(InvalidFilterException::class);
        $sut->getFilter(999);
    }
}
