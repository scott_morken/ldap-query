<?php

namespace Tests\LdapQuery\Unit\Results;

use DG\BypassFinals;
use LDAP\ResultEntry;
use LdapQuery\Contracts\Builder;
use LdapQuery\Contracts\Client;
use LdapQuery\Models\VO\Model;
use LdapQuery\Query\Builders\QueryBuilder;
use LdapQuery\Query\Filters\ActiveDirectory;
use LdapQuery\Results\Ldap;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class LdapTest extends TestCase
{

    protected Client|m\Mock|null $client = null;

    public function testFirst(): void
    {
        $sut = $this->getSut();
        $first = $this->getEntries()[0];
        $entries = [$first];
        $query = $this->getQueryBuilder()->setBaseDn('basedn');
        $sut->withQuery($query);
        $this->getClient()->shouldReceive('search')
             ->once()
             ->with('basedn', '', [])
             ->andReturn($entries);
        // can't mock LDAP\ResultEntry
        $this->getClient()->shouldReceive('firstEntry')
            ->once()
            ->with($entries)
            ->andReturn(false);
        $this->assertNull($sut->first());
    }

    public function testGet(): void
    {
        $entries = $this->getEntries();
        $sut = $this->getSut();
        $query = $this->getQueryBuilder()->setBaseDn('basedn');
        $sut->withQuery($query);
        $this->getClient()->shouldReceive('search')
            ->once()
            ->with('basedn', '', [])
            ->andReturn($entries);
        $this->getClient()->shouldReceive('getEntries')
            ->once()
            ->with($entries)
            ->andReturn($entries);
        $expected = [
            [
                'sn' => 'Newton',
                'uid' => 'newton',
                'mail' => 'newton@gmail.com',
                'cn' => 'Isaac Newton',
                'dn' => 'uid=newton,dc=domain,dc=org'
            ],
            [
                'sn' => 'Tesla',
                'uid' => 'tesla',
                'mail' => 'tesla@gmail.com',
                'cn' => 'Nikolai Tesla',
                'dn' => 'uid=tesla,dc=domain,dc=org'
            ]
        ];
        $models = $sut->get();
        foreach ($expected as $index => $attrs) {
            $model = $models[$index];
            foreach ($attrs as $key => $value) {
                $this->assertEquals($value, $model->getAttribute($key));
            }
        }
    }

    public function testParseOne(): void
    {
        $entry = $this->getEntries()[0];
        $sut = $this->getSut();
        $r = $sut->parse($entry);
        $this->assertEquals('Newton', $r['sn']);
        $this->assertCount(4, $r['objectclass']);
    }

    public function testParseWithoutCountIsEmpty(): void
    {
        $sut = $this->getSut();
        $this->assertEquals([], $sut->parse([]));
    }

    public function testSearchWithoutClientThrowsException(): void
    {
        $sut = new Ldap(new Model());
        $this->expectException('LdapQuery\LdapQueryException');
        $sut->withQuery(new QueryBuilder(new ActiveDirectory()))->get();
    }

    protected function getEntries(): array
    {
        return [
            'count' => 2,
            0 => [
                'sn' => [
                    'count' => 1,
                    0 => 'Newton',
                ],
                0 => 'sn',
                'objectclass' => [
                    'count' => 4,
                    0 => 'inetOrgPerson',
                    1 => 'organizationalPerson',
                    2 => 'person',
                    3 => 'top',
                ],
                1 => 'objectclass',
                'uid' => [
                    'count' => 1,
                    0 => 'newton',
                ],
                2 => 'uid',
                'mail' => [
                    'count' => 1,
                    0 => 'newton@gmail.com',
                ],
                3 => 'mail',
                'cn' => [
                    'count' => 1,
                    0 => 'Isaac Newton',
                ],
                4 => 'cn',
                'count' => 5,
                'dn' => 'uid=newton,dc=domain,dc=org',
            ],
            1 => [
                'sn' => [
                    'count' => 1,
                    0 => 'Tesla',
                ],
                0 => 'sn',
                'objectclass' => [
                    'count' => 4,
                    0 => 'inetOrgPerson',
                    1 => 'organizationalPerson',
                    2 => 'person',
                    3 => 'top',
                ],
                1 => 'objectclass',
                'uid' => [
                    'count' => 1,
                    0 => 'tesla',
                ],
                2 => 'uid',
                'mail' => [
                    'count' => 1,
                    0 => 'tesla@gmail.com',
                ],
                3 => 'mail',
                'cn' => [
                    'count' => 1,
                    0 => 'Nikolai Tesla',
                ],
                4 => 'cn',
                'count' => 5,
                'dn' => 'uid=tesla,dc=domain,dc=org',
            ],
        ];
    }

    protected function getQueryBuilder(): Builder
    {
        return new QueryBuilder(new ActiveDirectory());
    }

    protected function getClient(): Client
    {
        if (!$this->client) {
            $this->client = m::mock(Client::class);
        }
        return $this->client;
    }

    protected function getSut(): \LdapQuery\Contracts\Result
    {
        return (new Ldap(new \LdapQuery\Models\VO\Model()))
            ->withClient($this->getClient());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
