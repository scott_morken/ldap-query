<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/3/15
 * Time: 6:54 AM
 */

namespace LdapQuery\Models\VO;

class Model implements \LdapQuery\Contracts\Model
{

    protected array $attributes = [];

    public function __construct(array $attributes = [])
    {
        if (count($attributes)) {
            $this->setAttributes($attributes);
        }
    }

    public function __get(string $key): mixed
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, mixed $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function getAttribute(string $key): mixed
    {
        $key = strtolower($key);

        return array_key_exists($key, $this->attributes) ? $this->attributes[$key] : null;
    }

    public function newModel(array $attributes): static
    {
        return new static($attributes);
    }

    public function setAttribute(string $key, mixed $value): void
    {
        $key = strtolower($key);
        $this->attributes[$key] = $value;
    }

    public function setAttributes(array $attributes): void
    {
        foreach ($attributes as $k => $v) {
            $this->setAttribute($k, $v);
        }
    }

    public function toArray(array $attributes = []): array
    {
        if (count($attributes)) {
            $ret = [];
            foreach ($attributes as $k) {
                $ret[$k] = $this->getAttribute($k);
            }

            return $ret;
        }

        return $this->attributes;
    }
}
