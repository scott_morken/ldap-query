<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/2/15
 * Time: 3:13 PM
 */

namespace LdapQuery\Contracts;

interface Result
{

    public function first(): ?Model;

    /**
     * @return Model[]
     */
    public function get(): array;

    public function parse(array $entry): array;

    public function withClient(Client $client): self;

    public function withQuery(Builder $builder): self;
}
