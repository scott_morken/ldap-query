<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/3/15
 * Time: 6:54 AM
 */

namespace LdapQuery\Contracts;

interface Model
{

    public function getAttribute(string $key): mixed;

    public function newModel(array $attributes): static;

    public function setAttribute(string $key, mixed $value): void;

    public function setAttributes(array $attributes): void;

    public function toArray(array $attributes = []): array;
}
