<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/4/15
 * Time: 7:39 AM
 */

namespace LdapQuery\Contracts;

use LdapQuery\Query\Filters\InvalidFilterException;

interface Filter
{

    const AND_FILTER = 0x12;
    const APPROX_EQ_FILTER = 0x07;
    const CONTAINS_FILTER = 0x10;
    const ENDS_WITH_FILTER = 0x09;
    const EQUALS_FILTER = 0x02;
    const GTE_FILTER = 0x03;
    const GT_FILTER = 0x05;
    const LTE_FILTER = 0x04;
    const LT_FILTER = 0x06;
    const NOT_EQUALS_FILTER = 0x01;
    const OR_FILTER = 0x13;
    const STARTS_WITH_FILTER = 0x08;
    const WILDCARD_FILTER = 0x11;

    public function getAllowedFilters(): array;

    /**
     * Get the filter from the list of filters
     *
     * @throws InvalidFilterException
     */
    public function getFilter(int $filter = Filter::EQUALS_FILTER): string;

    public function getFilters(): array;

    /**
     * Is the filter allowed to be used?
     *
     * @throws InvalidFilterException
     */
    public function isAllowedFilter(int $filter): bool;
}
