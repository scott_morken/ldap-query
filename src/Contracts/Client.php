<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/2/15
 * Time: 9:57 AM
 */

namespace LdapQuery\Contracts;

use LDAP\Connection;
use LDAP\ResultEntry;
use const LDAP_DEREF_NEVER;

interface Client
{

    public function bind(?string $user = null, ?string $password = null): bool;

    public function close(): bool;

    public function connect(array|string $hosts, int $port = 389, array $options = []): bool;

    public function countEntries(\LDAP\Result|array|false $result): int;

    public function error(): string|false;

    public function errorNumber(): int|false;

    public function escape(string $value, string $ignore = '', int $flags = 0): string;

    public function firstEntry(\LDAP\Result|array|false $result): ResultEntry|false;

    public function getAttributes(ResultEntry|false $entry_rsrc): array|false;

    public function getConnection(): ?Connection;

    public function getDn(ResultEntry $entry_rsrc): string;

    public function getEntries(\LDAP\Result|array|false $result): array|false;

    public function getOption(int $option): mixed;

    public function getValues(ResultEntry|false $entry_rsrc, string $attribute): array|false;

    public function getValuesBinary(ResultEntry|false $entry_rsrc, string $attribute): array|false;

    public function isBound(): bool;

    public function isConnected(): bool;

    public function nextEntry(ResultEntry|false $entry_rsrc): ResultEntry|false;

    public function search(
        string $base_dn,
        string $filter,
        array $attrs = [],
        int $attrsonly = 0,
        int $sizelimit = 0,
        int $timelimit = 0,
        int $deref = LDAP_DEREF_NEVER
    ): array|false|\LDAP\Result;

    public function setOption(int $option, mixed $value): bool;

    public function startTls(): bool;

    public static function getDefaults(): array;
}
