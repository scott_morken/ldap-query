<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/1/15
 * Time: 2:48 PM
 */

namespace LdapQuery\Contracts;

interface Builder
{

    public function addOrWhere(string $attribute, ?int $operator, string|int|null $value): self;

    public function addSelect(string $attribute): self;

    public function addWhere(string $attribute, ?int $operator, string|int|null $value): self;

    public function getBaseDn(): ?string;

    public function getFilter(): Filter;

    public function getQuery(): string;

    public function getSelects(): array;

    public function getWheres(?string $type = null): array;

    public function hasSelects(): bool;

    public function newQuery(): Builder;

    public function orWhere(string $attribute, string|int|null $value = null, ?int $operator = null): self;

    public function select(array $attributes = []): self;

    public function setBaseDn(string $base_dn): self;

    public function setFilter(Filter $filter): void;

    public function where(string $attribute, string|int|null $value = null, ?int $operator = null): self;
}
