<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/1/15
 * Time: 2:33 PM
 */

namespace LdapQuery\Contracts;

interface Backend
{

    public function bind(?string $username = null, ?string $password = null): bool;

    public function connect(): bool;

    public function escape(string $value): string;

    public function findUser(Builder $query, string $username, array $attributes = []): Result;

    public function getClient(): Client;

    public function query(Builder $query): Result;

    public function strip(string $value): string;
}
