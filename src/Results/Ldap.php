<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/2/15
 * Time: 3:13 PM
 */

namespace LdapQuery\Results;

use LdapQuery\Contracts\Builder;
use LdapQuery\Contracts\Client;
use LdapQuery\Contracts\Model;
use LdapQuery\Contracts\Result;
use LdapQuery\LdapQueryException;

class Ldap implements Result
{

    protected ?Builder $builder = null;

    protected ?Client $client = null;

    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function first(): ?Model
    {
        $results = $this->search($this->builder);
        if ($results) {
            $entry = $this->client->firstEntry($results);
            if ($entry) {
                $attributes = $this->client->getAttributes($entry);

                return $this->newModelFromEntry($attributes);
            }
        }

        return null;
    }

    public function get(): array
    {
        $results = $this->search($this->builder);
        $parsed = [];
        if ($results) {
            $entries = $this->client->getEntries($results);
            if ($entries) {
                foreach ($entries as $i => $entry) {
                    if ($i === 'count') {
                        continue;
                    }
                    $parsed[] = $this->newModelFromEntry($entry);
                }
            }
        }

        return $parsed;
    }

    public function parse(array $entry, bool $subtree = false): array
    {
        $ret = [];
        foreach ($entry as $attr => $value) {
            if ($attr === 'count') {
                continue;
            }
            if (! $subtree && is_numeric($attr)) {
                continue;
            }
            $count = (is_array($value) && array_key_exists('count', $value) ? $value['count'] : 0);
            if (! is_array($value)) {
                $ret[$attr] = $value;
            } else {
                if ($count === 1) {
                    $ret[$attr] = $value[0];
                } else {
                    $ret[$attr] = $this->parse($value, true);
                }
            }
        }

        return $ret;
    }

    public function withClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function withQuery(Builder $builder): self
    {
        $this->builder = $builder;

        return $this;
    }

    protected function newModelFromEntry(array $entry): Model
    {
        return $this->model->newModel($this->parse($entry));
    }

    protected function search(Builder $builder): array|false|\LDAP\Result
    {
        $this->validateRequest();
        $base_dn = $builder->getBaseDn();
        $attributes = $builder->getSelects();
        $filter = $builder->getQuery();

        return $this->client->search($base_dn, $filter, $attributes);
    }

    protected function validateRequest(): void
    {
        if (! $this->client) {
            throw new LdapQueryException('A client is required.');
        }
        if (! $this->builder) {
            throw new LdapQueryException('A query is required.');
        }
    }
}
