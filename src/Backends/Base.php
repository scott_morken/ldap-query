<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/1/15
 * Time: 2:58 PM
 */

namespace LdapQuery\Backends;

use LdapQuery\Contracts\Builder;
use LdapQuery\Contracts\Client;
use LdapQuery\Contracts\Result;
use LdapQuery\LdapQueryException;
use LdapQuery\Traits\SetOptionsViaDefaults;

abstract class Base
{

    use SetOptionsViaDefaults;

    protected Client $client;

    protected Result $result;

    public function __construct(Client $client, Result $result, array $options = [])
    {
        $this->client = $client;
        $this->result = $result;
        $this->setOptionsViaDefaults($options);
    }

    public static function getDefaults(): array
    {
        return [
            'host' => 'localhost',
            'port' => 389,
            'base_dn' => 'dc=domain,dc=org',
            'bind_user' => null,
            'bind_password' => null,
            'rebind' => true,
            'follow_referrals' => 1,
            'bind_filter' => '%s@domain.org',
            'client_options' => [
                'ssl' => false,
                'start_tls' => false,
                'default_port' => 389,
                'default_ssl_port' => 636,
                'default_ssl_prefix' => 'ldaps://',
                'default_prefix' => 'ldap://',
            ],
            'filters' => [
                'person' => [
                    'category' => 'objectCategory',
                    'category_value' => 'person',
                    'uid_attribute' => 'anr',
                    'samaccounttype' => 805306368,
                ],
            ],
        ];
    }

    /**
     * @throws LdapQueryException
     */
    public function bind(?string $username = null, ?string $password = null): bool
    {
        $connected = $this->connect();
        if ($connected) {
            $username = $this->validateUsername($username);
            $bound = $this->getClient()
                ->bind($username, $password);
            $rebind = $this->getOptionByKey('rebind', true);
            if ($bound && $rebind) {
                $this->rebind(true);
            }

            return $bound;
        }

        return false;
    }

    public function connect(): bool
    {
        if (! $this->getClient()
            ->isConnected()) {
            $r = $this->getClient()
                ->connect(
                    $this->getHosts(),
                    $this->getOptionByKey('port', 389),
                    $this->getOptionByKey('client_options', [])
                );
            if ($r) {
                $this->getClient()
                    ->setOption(LDAP_OPT_PROTOCOL_VERSION, 3);
                $this->getClient()
                    ->setOption(LDAP_OPT_NETWORK_TIMEOUT, 10);
                $this->getClient()
                    ->setOption(LDAP_OPT_REFERRALS, $this->getOptionByKey('follow_referrals', 1));
            }
        }

        return $this->getClient()
            ->isConnected();
    }

    public function escape(string $value): string
    {
        return $this->getClient()
            ->escape($value);
    }

    /**
     * @throws LdapQueryException
     */
    public function findUser(Builder $query, string $username, array $attributes = []): Result
    {
        if (! $username) {
            throw new LdapQueryException('Username cannot be null.');
        }
        $filters = $this->getOptionByKey('filters', []);
        $person = $this->getOptionFromArray($filters, 'person', []);
        $query->select($attributes)
            ->where($this->getOptionFromArray($person, 'uid_attribute', 'anr'), $this->escape($username));
        if ($this->getOptionFromArray($person, 'samaccounttype', false)) {
            $query->where('samaccounttype', $this->escape($this->getOptionFromArray($person, 'samaccounttype', 0)));
        }
        if ($this->getOptionFromArray($person, 'category', false)) {
            $query->where(
                $this->getOptionFromArray($person, 'category', 'objectCategory'),
                $this->escape($this->getOptionFromArray($person, 'category_value', 'person'))
            );
        }

        return $this->query($query);
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @throws LdapQueryException
     */
    public function query(Builder $query): Result
    {
        $this->connect();
        $this->rebind();
        $this->validateQueryBuilder($query);

        return $this->result->withClient($this->getClient())
            ->withQuery($query);
    }

    public function strip(string $value): string
    {
        $badchars = ['\\', '*', '(', ')', "\x00"];

        return str_ireplace($badchars, '', $value);
    }

    protected function getHosts(): array
    {
        $host = $this->getOptionByKey('host', 'localhost');
        $hosts = array_map('trim', explode(',', $host));

        return $hosts;
    }

    protected function rebind(bool $force = false): void
    {
        if ($this->getClient()
                ->isConnected() && (! $this->getClient()
                    ->isBound() || $force)) {
            $username = $this->validateUsername($this->getOptionByKey('bind_user', null));
            if (! $this->getClient()
                ->bind($username, $this->getOptionByKey('bind_password', null))) {
                throw new LdapQueryException(
                    'Unable to rebind: '.$this->getClient()
                        ->error()
                );
            }
        }
    }

    protected function validateQueryBuilder(Builder $query): void
    {
        if (! $query->getBaseDn()) {
            $query->setBaseDn($this->getOptionByKey('base_dn', 'dc=domain,dc=org'));
        }
    }

    protected function validateUsername(?string $username): ?string
    {
        if ($username !== null && stripos($username, 'dc=') === false && ! str_contains($username, '@')) {
            return sprintf($this->getOptionByKey('bind_filter', '%s'), $username);
        }

        return $username;
    }
}
