<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/4/15
 * Time: 7:42 AM
 */

namespace LdapQuery\Query\Filters;

use LdapQuery\Contracts\Filter;

class ActiveDirectory extends Base implements Filter
{

    public function getAllowedFilters(): array
    {
        return [
            Filter::NOT_EQUALS_FILTER,
            Filter::EQUALS_FILTER,
            Filter::GTE_FILTER,
            Filter::LTE_FILTER,
            Filter::GT_FILTER,
            Filter::LT_FILTER,
            Filter::APPROX_EQ_FILTER,
            Filter::STARTS_WITH_FILTER,
            Filter::ENDS_WITH_FILTER,
            Filter::CONTAINS_FILTER,
            Filter::WILDCARD_FILTER,
        ];
    }

    public function getFilters(): array
    {
        return [
            Filter::NOT_EQUALS_FILTER => '(!(%s=%s))',
            Filter::EQUALS_FILTER => '(%s=%s)',
            Filter::GTE_FILTER => '(%s>=%s)',
            Filter::LTE_FILTER => '(%s<=%s)',
            Filter::GT_FILTER => '(%s>%s)',
            Filter::LT_FILTER => '(%s<%s)',
            Filter::APPROX_EQ_FILTER => '(%s~=%s)',
            Filter::STARTS_WITH_FILTER => '(%s=%s*)',
            Filter::ENDS_WITH_FILTER => '(%s=*%s)',
            Filter::CONTAINS_FILTER => '(%s=*%s*)',
            Filter::WILDCARD_FILTER => '(%s=*)',
            Filter::AND_FILTER => '(&%s)',
            Filter::OR_FILTER => '(|%s)',
        ];
    }
}
