<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/4/15
 * Time: 7:51 AM
 */

namespace LdapQuery\Query\Filters;

use LdapQuery\Contracts\Filter;

abstract class Base implements Filter
{

    /**
     * @throws InvalidFilterException
     */
    public function getFilter(int $filter = Filter::EQUALS_FILTER): string
    {
        if (array_key_exists($filter, $this->getFilters())) {
            return $this->getFilters()[$filter];
        }
        throw new InvalidFilterException("$filter is not a valid filter.");
    }

    /**
     * @throws InvalidFilterException
     */
    public function isAllowedFilter(int $filter): bool
    {
        if (in_array($filter, $this->getAllowedFilters(), true)) {
            return true;
        }
        throw new InvalidFilterException("$filter is not allowed in this context.");
    }
}
