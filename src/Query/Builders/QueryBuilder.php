<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/4/15
 * Time: 7:54 AM
 */

namespace LdapQuery\Query\Builders;

use LdapQuery\Contracts\Builder;
use LdapQuery\Contracts\Filter;

class QueryBuilder implements Builder
{

    const WHERE_AND = 'AND';

    const WHERE_OR = 'OR';

    protected ?string $base_dn = null;

    protected Filter $filter;

    protected array $selects = [];

    protected array $wheres = [];

    public function __construct(Filter $filter)
    {
        $this->setFilter($filter);
    }

    public function addOrWhere(string $attribute, ?int $operator, string|int|null $value): self
    {
        $this->_addWhere($attribute, $operator, $value, self::WHERE_OR);

        return $this;
    }

    public function addSelect(string $attribute): self
    {
        $this->selects[] = $attribute;

        return $this;
    }

    public function addWhere(string $attribute, ?int $operator, string|int|null $value): self
    {
        $this->_addWhere($attribute, $operator, $value, self::WHERE_AND);

        return $this;
    }

    public function getBaseDn(): ?string
    {
        return $this->base_dn;
    }

    public function setBaseDn(string $base_dn): self
    {
        $this->base_dn = $base_dn;

        return $this;
    }

    public function getFilter(): Filter
    {
        return $this->filter;
    }

    public function setFilter(Filter $filter): void
    {
        $this->filter = $filter;
    }

    public function getQuery(): string
    {
        return $this->assembleQuery();
    }

    public function getSelects(): array
    {
        return $this->selects;
    }

    public function getWheres(?string $type = null): array
    {
        if ($type === null) {
            return $this->wheres;
        } else {
            return array_filter(
                $this->wheres,
                function ($v) use ($type) {
                    return $v['type'] === $type;
                }
            );
        }
    }

    public function hasSelects(): bool
    {
        return count($this->selects) > 0;
    }

    public function newQuery(): static
    {
        return new static($this->getFilter());
    }

    public function orWhere(string $attribute, string|int|null $value = null, ?int $operator = null): self
    {
        $this->addOrWhere($attribute, $operator, $value);

        return $this;
    }

    public function select(array $attributes = []): self
    {
        if (is_array($attributes)) {
            foreach ($attributes as $attribute) {
                $this->addSelect($attribute);
            }
        } else {
            $this->addSelect($attributes);
        }

        return $this;
    }

    public function where(string $attribute, string|int|null $value = null, ?int $operator = null): self
    {
        $this->addWhere($attribute, $operator, $value);

        return $this;
    }

    protected function _addWhere(string $attribute, ?int $filter, string|int|null $value, ?string $type): self
    {
        $this->wheres[] = [
            'attribute' => $attribute,
            'filter' => $this->validateFilter($filter),
            'value' => $value,
            'type' => $type,
        ];

        return $this;
    }

    protected function applyFilter(int $filter, array $args): string
    {
        array_unshift($args, $this->getFilterString($filter));

        return call_user_func_array('sprintf', $args);
    }

    protected function assembleQuery(): string
    {
        $query = '';
        $query = $this->assembleWheres($this->getWheres(self::WHERE_AND), $query);
        $query = $this->assembleWheres($this->getWheres(self::WHERE_OR), $query, Filter::OR_FILTER);
        if (count($this->getWheres()) > 1) {
            $query = $this->applyFilter(Filter::AND_FILTER, [$query]);
        }

        return $query;
    }

    protected function assembleWhere(array $where): string
    {
        return match ($where['filter']) {
            Filter::WILDCARD_FILTER => $this->wildcardFilter($where['attribute']),
            default => $this->applyFilter($where['filter'], [$where['attribute'], $where['value']]),
        };
    }

    protected function assembleWheres(array $wheres, string $query, ?string $wrap = null): string
    {
        if (count($wheres)) {
            $ws = '';
            foreach ($wheres as $where) {
                $ws .= $this->assembleWhere($where);
            }
            if ($wrap !== null) {
                $ws = $this->applyFilter($wrap, [$ws]);
            }
            $query .= $ws;
        }

        return $query;
    }

    protected function getFilterString(int $filter): string
    {
        return $this->getFilter()->getFilter($filter);
    }

    protected function validateFilter(?int $filter): int|false
    {
        if ($filter === null) {
            $filter = Filter::EQUALS_FILTER;
        }
        if ($this->getFilter()->isAllowedFilter($filter)) {
            return $filter;
        }

        return false;
    }

    protected function wildcardFilter(string $attribute): string
    {
        return $this->applyFilter(Filter::WILDCARD_FILTER, [$attribute]);
    }
}
