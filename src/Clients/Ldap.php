<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/2/15
 * Time: 10:29 AM
 */

namespace LdapQuery\Clients;

use LDAP\Connection;
use LDAP\Result;
use LDAP\ResultEntry;
use LdapQuery\Contracts\Client;
use LdapQuery\Traits\SetOptionsViaDefaults;

/**
 * Class Ldap
 *
 * @package LdapQuery\Clients
 * Interface between ldap_ functions and LdapQuery library
 */
class Ldap implements Client
{

    use SetOptionsViaDefaults;

    protected bool $bound = false;

    protected ?Connection $connection = null;

    public static function getDefaults(): array
    {
        return [
            'ssl' => false,
            'start_tls' => false,
            'default_port' => 389,
            'default_ssl_port' => 636,
            'default_ssl_prefix' => 'ldaps://',
            'default_prefix' => 'ldap://',
        ];
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * @throws ClientException
     */
    public function bind(?string $user = null, ?string $password = null): bool
    {
        $tries = $this->getOptionByKey('retry_count', 2);
        $usleep = $this->getOptionByKey('retry_usleep', 1000);
        $this->bound = $this->recurseTry('ldap_bind', [$this->getConnection(), $user, $password], $tries, $usleep);
        $e = $this->errorNumber();
        if ($e !== false && $e !== 49) {
            throw new ClientException($this->error(), $e);
        }

        return $this->isBound();
    }

    /**
     * @return bool
     */
    public function close(): bool
    {
        if ($this->isConnected()) {
            ldap_close($this->connection);
        }
        $this->connection = null;
        $this->bound = false;

        return $this->bound;
    }

    /**
     * @throws ClientException
     */
    public function connect(array|string $hosts, int $port = 389, array $options = []): bool
    {
        if (! $this->isConnected()) {
            if (count($options)) {
                $this->setOptionsViaDefaults($options);
            }
            $this->setPreConnectOptions();
            if (! is_array($hosts)) {
                $hosts = explode(' ', $hosts);
            }
            $ldap_uri = $this->createLdapUri($hosts, $port);
            $r = $this->tryConnect($ldap_uri);
            $this->setPostConnectOptions($r);
            $this->tryStartTls($r);
            $this->connection = $r;
        }

        return $this->isConnected();
    }

    public function countEntries(Result|array|false $result): int
    {
        return ldap_count_entries($this->getConnection(), $result);
    }

    public function error(): string|false
    {
        return $this->errorFromConn($this->getConnection());
    }

    public function errorNumber(): int|false
    {
        return $this->errorNumberFromConn($this->getConnection());
    }

    public function escape(string $value, string $ignore = '', int $flags = 0): string
    {
        return ldap_escape($value, $ignore, $flags);
    }

    public function firstEntry(Result|array|false $result): ResultEntry|false
    {
        return ldap_first_entry($this->getConnection(), $result);
    }

    public function getAttributes(ResultEntry|false $entry_rsrc): array|false
    {
        return ldap_get_attributes($this->getConnection(), $entry_rsrc);
    }

    public function getConnection(): ?Connection
    {
        return $this->connection;
    }

    public function getDn(ResultEntry $entry_rsrc): string
    {
        return ldap_get_dn($this->getConnection(), $entry_rsrc);
    }

    public function getEntries(Result|array|false $result): array|false
    {
        return ldap_get_entries($this->getConnection(), $result);
    }

    public function getOption(int $option): mixed
    {
        $retval = null;
        ldap_get_option($this->getConnection(), $option, $retval);

        return $retval;
    }

    public function getValues(ResultEntry|false $entry_rsrc, string $attribute): array|false
    {
        return ldap_get_values($this->getConnection(), $entry_rsrc, $attribute);
    }

    public function getValuesBinary(ResultEntry|false $entry_rsrc, string $attribute): array|false
    {
        return ldap_get_values_len($this->getConnection(), $entry_rsrc, $attribute);
    }

    public function isBound(): bool
    {
        return $this->bound;
    }

    public function isConnected(): bool
    {
        return $this->connection instanceof Connection;
    }

    public function nextEntry(ResultEntry|false $entry_rsrc): ResultEntry|false
    {
        return ldap_next_entry($this->getConnection(), $entry_rsrc);
    }

    public function search(
        string $base_dn,
        string $filter,
        array $attrs = [],
        int $attrsonly = 0,
        int $sizelimit = 0,
        int $timelimit = 0,
        int $deref = LDAP_DEREF_NEVER
    ): array|false|Result {
        return @ldap_search($this->getConnection(), $base_dn, $filter, $attrs, $attrsonly, $sizelimit, $timelimit,
            $deref);
    }

    public function setOption(int $option, mixed $value): bool
    {
        return ldap_set_option($this->getConnection(), $option, $value);
    }

    public function startTls(): bool
    {
        return ldap_start_tls($this->getConnection());
    }

    protected function createLdapUri(array $hosts, int $port): string
    {
        $ldap_uris = [];
        $port = (int) $port;
        foreach ($hosts as $host) {
            $host = $this->validateHost($host, $port);
            $port = $this->validatePort($port);
            $ldap_uris[] = sprintf('%s:%d', $host, $port);
        }

        return implode(' ', $ldap_uris);
    }

    protected function errorFromConn(Connection $conn): string|false
    {
        $e = ldap_error($conn);
        $ex = null;
        ldap_get_option($conn, LDAP_OPT_DIAGNOSTIC_MESSAGE, $ex);
        if ($ex && $ex !== '(unknown error code)') {
            $e = sprintf('%s - extended: %s', $e, $ex);
        }

        return $e ?: false;
    }

    protected function errorNumberFromConn(Connection $conn): int|false
    {
        $e = ldap_errno($conn);

        return $e ?: false;
    }

    protected function getLdapUri(string $host, int $port): string
    {
        return sprintf('%s:%d', $host, $port);
    }

    protected function recurseTry(
        string $func_name,
        array $parameters,
        int $count = 2,
        int $usleep = 1000,
        int $current = 0
    ): mixed {
        if (! function_exists($func_name)) {
            throw new ClientException("$func_name does not exist.");
        }
        $result = false;
        if ($current >= $count) {
            return $result;
        }
        $result = @call_user_func_array($func_name, $parameters);
        if (! $result) {
            usleep($usleep);
            $current = $current + 1;
            $result = $this->recurseTry($func_name, $parameters, $count, $usleep, $current);
        }

        return $result;
    }

    protected function setPostConnectOptions(Connection $conn): void
    {
        foreach ($this->getOptionByKey('post_connect_options', []) as $option => $value) {
            $const = defined($option) ? constant($option) : $option;
            $this->setOption($const, $value);
        }
    }

    protected function setPreConnectOptions(): void
    {
        foreach ($this->getOptionByKey('pre_connect_options', []) as $option => $value) {
            $const = defined($option) ? constant($option) : $option;
            ldap_set_option(null, $const, $value);
        }
    }

    protected function throwClientExceptionFromError(Connection $conn, ?string $alt_message = null): void
    {
        $e = $this->errorNumberFromConn($conn);
        if ($e !== false && $e !== 49) {
            throw new ClientException($this->errorFromConn($conn), $e);
        }
        if ($alt_message) {
            throw new ClientException($alt_message);
        }
    }

    protected function tryConnect(string $ldap_uri): Connection
    {
        $tries = $this->getOptionByKey('retry_count', 2);
        $usleep = $this->getOptionByKey('retry_usleep', 1000);
        $r = $this->recurseTry('ldap_connect', [$ldap_uri], $tries, $usleep);
        if ($r === false) {
            throw new ClientException(sprintf('Unable to connect to %s.', $ldap_uri));
        }

        return $r;
    }

    protected function tryStartTls(Connection $conn): bool
    {
        $ok = true;
        if (is_resource($conn) && $this->getOptionByKey('start_tls', false)) {
            $tries = $this->getOptionByKey('retry_count', 2);
            $usleep = $this->getOptionByKey('retry_usleep', 1000);
            $ok = $this->recurseTry('ldap_start_tls', [$conn], $tries, $usleep);
        }
        if ($ok === false) {
            $this->throwClientExceptionFromError($conn, 'Unable to start TLS.');
        }

        return $ok;
    }

    protected function validateHost(string $host, int $port): string
    {
        $def_pre = $this->getOptionByKey('default_prefix', 'ldap://');
        $def_ssl_pre = $this->getOptionByKey('default_ssl_prefix', 'ldaps://');
        $host = str_ireplace([
            $def_pre,
            $def_ssl_pre,
        ], '', $host);
        if ($this->getOptionByKey('start_tls', false)) {
            return sprintf('%s%s', $def_pre, $host);
        }
        if ($this->getOptionByKey('ssl', false) || $port === (int) $this->getOptionByKey('default_ssl_port', 636)) {
            $this->options['ssl'] = true;

            return sprintf('%s%s', $def_ssl_pre, $host);
        }

        return sprintf('%s%s', $def_pre, $host);
    }

    protected function validatePort(int $port): int
    {
        $default_ssl_port = (int) $this->getOptionByKey('default_ssl_port', 636);
        $default_port = (int) $this->getOptionByKey('default_port', 389);
        if ($port === $default_ssl_port && $this->getOptionByKey('start_tls', false)) {
            throw new ClientException('SSL port selected with start TLS option.');
        }
        if ($port !== $default_ssl_port && $this->getOptionByKey('ssl', false)) {
            $port = $default_ssl_port;
        } else {
            if ($port !== $default_port && ! $this->getOptionByKey('ssl', false)) {
                $port = $default_port;
            }
        }

        return $port;
    }
}
