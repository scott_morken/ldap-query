<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/2/15
 * Time: 10:52 AM
 */

namespace LdapQuery\Traits;

trait SetOptionsViaDefaults
{

    protected array $options = [];

    public static function getDefaults(): array
    {
        return [];
    }

    public function getOptionByKey(string $key, mixed $default = null): mixed
    {
        return $this->getOptionFromArray($this->options, $key, $default);
    }

    public function getOptionFromArray(array $arr, string $key, mixed $default = null): mixed
    {
        return array_key_exists($key, $arr) ? $arr[$key] : $default;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptionByKey(string $key, mixed $value): void
    {
        $this->options[$key] = $value;
    }

    public function setOptionsViaDefaults(array $config = []): void
    {
        $this->options = array_replace_recursive(self::getDefaults(), $config);
    }
}
