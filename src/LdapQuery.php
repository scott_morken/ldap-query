<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/1/15
 * Time: 2:32 PM
 */

namespace LdapQuery;

use LdapQuery\Contracts\Backend;
use LdapQuery\Contracts\Builder;
use LdapQuery\Contracts\Model;
use LdapQuery\Contracts\Result;

class LdapQuery
{

    protected Backend $backend;

    protected Builder $builder;

    public function __construct(Backend $backend, Builder $builder)
    {
        $this->setBackend($backend);
        $this->setBuilder($builder);
    }

    /**
     * @return Contracts\Model[]
     */
    public function all(string $attribute, string $value): array
    {
        return $this->getBackend()->query($this->_query($attribute, $value))->get();
    }

    public function authenticate(?string $username, ?string $password): bool
    {
        return $this->getBackend()->bind($username, $password);
    }

    public function escape(string|int $value): string
    {
        $value = (string) $value;

        return $this->getBackend()->escape($value);
    }

    public function execute(Builder $query): Result
    {
        return $this->getBackend()->query($query);
    }

    /**
     * @throws \LdapQuery\LdapQueryException
     */
    public function findUser(string $username, array $attributes = []): ?Model
    {
        return $this->getBackend()->findUser($this->newQuery(), $username, $attributes)->first();
    }

    public function getBackend(): Backend
    {
        return $this->backend;
    }

    public function setBackend(Backend $backend): void
    {
        $this->backend = $backend;
    }

    public function getBuilder(): Builder
    {
        return $this->builder;
    }

    public function setBuilder(Builder $builder): void
    {
        $this->builder = $builder;
    }

    public function newQuery(): Builder
    {
        return $this->getBuilder()->newQuery();
    }

    public function one(string $attribute, string $value): ?Model
    {
        return $this->getBackend()->query($this->_query($attribute, $value))->first();
    }

    public function strip(string|int $value): string
    {
        $value = (string) $value;

        return $this->getBackend()->strip($value);
    }

    protected function _query(string $attribute, string|int $value): Builder
    {
        $attribute = $this->escape($attribute);
        $value = $this->escape($value);

        return $this->getBuilder()->newQuery()->where($attribute, $value);
    }
}
