<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/3/15
 * Time: 9:21 AM
 */

namespace LdapQuery\Opinionated;

use LdapQuery\LdapQuery;
use LdapQuery\Models\VO\Model;
use LdapQuery\Query\Builders\QueryBuilder;
use LdapQuery\Results\Ldap;

class ActiveDirectory
{

    protected LdapQuery $ldap_query;

    public function __construct(array $options = [])
    {
        $backend = new \LdapQuery\Backends\ActiveDirectory(
            new \LdapQuery\Clients\Ldap(),
            new Ldap(new Model()),
            $options
        );
        $builder = new QueryBuilder(new \LdapQuery\Query\Filters\ActiveDirectory());
        $this->ldap_query = new LdapQuery($backend, $builder);
    }

    public function getLdapQuery(): LdapQuery
    {
        return $this->ldap_query;
    }
}
